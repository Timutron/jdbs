package demo.JdbsConector.Repositori;

import demo.JdbsConector.Resurses.User;

import java.util.List;

public interface IUsersRepo {

    public List<User> findAll();

    public User getById(String user_id);
}
