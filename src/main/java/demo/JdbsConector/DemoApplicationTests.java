package demo.JdbsConector;

import demo.JdbsConector.Service.IUserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class DemoApplicationTests {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        IUserService iuserService = context.getBean(IUserService.class);
        iuserService.getUserById("2");
        iuserService.showAll();
    }
}