package demo.JdbsConector.Service;

import demo.JdbsConector.Repositori.IUsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService implements IUserService {

    @Autowired
    IUsersRepo iUsersRepo;

    @Override
    public void showAll() {
        System.out.println(iUsersRepo.findAll());
    }

    @Override
    public void getUserById(String user_id) {
        System.out.println(iUsersRepo.getById(user_id));
    }
}
